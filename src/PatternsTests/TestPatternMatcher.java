package PatternsTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import Patterns.PatternMatcher;

public class TestPatternMatcher {

	private final String testPatterns = "src\\patterns.txt";
	private final String originalTestText = "src\\PatternsTests\\testText1.txt";//original text
	private final String testText2 = "src\\PatternsTests\\testText2.txt";//test text, a little longer
	private final String testText3 = "src\\PatternsTests\\testText3.txt";//test text, short
	private final String testText1ExpectedResult1 = 
			"src\\PatternsTests\\testText1ExpRes1.txt"; //pattern 1 file 1
	private final String testText1ExpectedResult2 = 
			"src\\PatternsTests\\testText1ExpRes2.txt"; //pattern 2 file 1
	private final String testText1ExpectedResult3 = 
			"src\\PatternsTests\\testText1ExpRes3.txt"; //pattern 3 file 1
	private final String testText2ExpectedResult1 = 
			"src\\PatternsTests\\testText2ExpRes1.txt"; //pattern 1 file 2
	private final String testText2ExpectedResult2 = 
			"src\\PatternsTests\\testText2ExpRes2.txt"; //pattern 2 file 2
	private final String testText2ExpectedResult3 = 
			"src\\PatternsTests\\testText2ExpRes3.txt"; //pattern 3 file 2
	private final String testText3ExpectedResult1 = 
			"src\\PatternsTests\\testText3ExpRes1.txt"; //pattern 1 file 3
	private final String testText3ExpectedResult2 = 
			"src\\PatternsTests\\testText3ExpRes2.txt"; //pattern 2 file 3
	private final String testText3ExpectedResult3 = 
			"src\\PatternsTests\\testText3ExpRes3.txt"; //pattern 3 file 3


	@Test
	public void testInitialization() throws IOException{
		PatternMatcher pm = new PatternMatcher(originalTestText, testPatterns, 1);
		assertTrue("Dictionary should be empty at initialization.", pm.getDictionary().isEmpty());
	}
	
	//testing output with pattern 1 against original text
	@Test
	public void testOriginalTextPattern1() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(originalTestText, testPatterns, 1);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText1ExpectedResult1));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	//testing output with pattern 2 against original text
	@Test
	public void testOriginalTextPattern2() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(originalTestText, testPatterns, 2);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText1ExpectedResult2));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	
	//testing output with pattern 3 against original text
	@Test
	public void testOriginalTextPattern3() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(originalTestText, testPatterns, 3);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText1ExpectedResult3));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	
	//testing output with pattern 1 against test text 2
	@Test
	public void testText2Pattern1() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(testText2, testPatterns, 1);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText2ExpectedResult1));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	//testing output with pattern 2 against test text 2
	@Test
	public void testText2Pattern2() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(testText2, testPatterns, 2);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText2ExpectedResult2));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	
	//testing output with pattern 3 against test text 2
	@Test
	public void testText2Pattern3() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(testText2, testPatterns, 3);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText2ExpectedResult3));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	
	//testing output with pattern 1 against test text 3
	@Test
	public void testText3Pattern1() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(testText3, testPatterns, 1);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText3ExpectedResult1));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	//testing output with pattern 2 against test text 3
	@Test
	public void testText3Pattern2() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(testText3, testPatterns, 2);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText3ExpectedResult2));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}
	
	//testing output with pattern 3 against test text 3
	@Test
	public void testText3Pattern3() throws IOException{ 
		//pattern 0
		PatternMatcher pm = new PatternMatcher(testText3, testPatterns, 3);
		
		pm.countPattern();
		
		List<String> expectedResultLines = Files.readAllLines(Paths.get(testText3ExpectedResult3));
		String expectedResult = expectedResultLines.stream().collect(Collectors.joining(System.lineSeparator()));
		
		String result = pm.getDictionary().toString();
		
		assertEquals("Strings of output and expected results should be the same.", expectedResult, result);		
	}

}
