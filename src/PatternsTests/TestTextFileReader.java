package PatternsTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Patterns.TextFileReader;

public class TestTextFileReader {
	
	private final String testFile1 = "src\\PatternsTests\\testFile1.txt";
	
	//private Path testFile;
	private TextFileReader tfr;
	List<String> testFileLines;

	@Before
	public void setUp() throws Exception {
		testFileLines = Files.readAllLines(Paths.get(testFile1));

	}

	@Test
	public void testReadIn() throws IOException{
		tfr = new TextFileReader(testFile1);
		assertFalse(tfr.getFileText().isEmpty());
		assertFalse(tfr.getLine(0).isEmpty());
	}
	
	@Test
	public void testLineVsFullFile() throws IOException{
		tfr = new TextFileReader(testFile1);
		
		String[] fileText = tfr.getFileText().split(System.lineSeparator());
		
		int lineCounter = 0;
		for(String line: fileText){
			
			assertEquals("Each line of full file text equals result of getLine()", line, tfr.getLine(lineCounter));
			assertEquals("Each line should be equal to that read in in tester", line, testFileLines.get(lineCounter));
			lineCounter++;
		}
		
	}

}
