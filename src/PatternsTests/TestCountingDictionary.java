package PatternsTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Patterns.CountingDictionary;

public class TestCountingDictionary {
	
	private String testFilePath = "src\\PatternsTests\\TestCountingDictionary.txt";
	private final String[] entries = {"one","two","three","four"};
	private CountingDictionary cd;

	@Before
	public void setUp() throws Exception {
		cd = new CountingDictionary();
	}

	@Test
	public void testIsEmpty() {
		assertTrue("Empty at beginning.", cd.isEmpty());
		cd.put("one");
		assertFalse("Not empty after insertion.", cd.isEmpty());
	}
	
	@Test
	public void testNonExistentEntry(){
		cd.put("one");
		cd.put("two");
		assertTrue("Look up key which is there.", cd.containsKey("one"));
		assertFalse("Look up key that isn't there.",cd.containsKey("three"));
		assertTrue("Get value for key which isn't there.", cd.get("three")==null);
	}
	
	@Test
	public void testPutContains() {
		cd.put(entries[0]);
		cd.put(entries[1]);
		cd.put(entries[3]);
		
		assertTrue("Contains " + entries[0] +  ".", cd.containsKey(entries[0]));
		assertTrue("Contains " + entries[1] +  ".", cd.containsKey(entries[1]));
		assertFalse("Does not contain " + entries[2] +  ".", cd.containsKey(entries[2]));
		assertTrue("Contains " + entries[3] +  ".", cd.containsKey(entries[3]));
	}
	
	@Test
	public void testRecallOrder() throws IOException{
		List<String> testFileLines = Files.readAllLines(Paths.get(testFilePath));
		String[] kv = null;
		//for each file line, put some data into the CountingDictionary
		for(String str: testFileLines){
			kv = str.split(", ");
			//loop over the value of entries supposed to be there (second value in each row)
			for(int i = 0; i < Integer.parseInt(kv[1]); i++){
				//put the key in each time
				cd.put(kv[0]);
			}
		}
		

		String output = cd.toString();
		
		assertTrue("Output string is not null.", output!=null);				
		
		String[] outputLines = output.split(System.lineSeparator());
		
		int outputIteration = 0;
		
		for(String expectedStr: testFileLines){
			assertEquals("Line " + outputIteration + " equal to test.", expectedStr, outputLines[outputIteration]);
			outputIteration++;
		}
		
	}
	
	@Test
	public void testCounting(){
		for(int i = 1; i < 10; i++){
			for(String str: entries){
				cd.put(str);
				assertEquals("Testing several elemens over several insertions: ", i,cd.get(str).intValue());
			}
		}
	}

}
