package PatternsTests;

import junit.framework.TestCase;

import org.junit.Test;

import Patterns.PatternCounter;

public class TestPatternCounter extends TestCase{
	
	private final String testText = "src\\PatternsTests\\testText1.txt";

	 //Note: There is a nice little package which allows testing of programs which call System.exit(),
	 //and which allows testing against text output to the console. I kept this simple, and did not 
	 //include lots of extraneous packages, though obviously, much more could be included for added
	 //complexity, and niceties.
	@Test
	public void testPattern1Text1() {
		System.out.println("Pattern 1 test.");
		String[] args = {testText, "1"};
		PatternCounter.main(args);
		//if it makes it this far, good
		assertTrue("Made it through first test.", 1==Integer.parseInt(args[1]));
	}
	//check console output - should be same as test files, more complex situations tested in TestPatternMatcher
	@Test
	public void testPattern2Text1() {
		System.out.println("Pattern 2 test.");
		String[] args = {testText, "2"};
		PatternCounter.main(args);
		assertTrue(true);
	}
	
	@Test
	public void testPattern3Text1() {
		System.out.println("Pattern 3 test.");
		String[] args = {testText, "3"};
		PatternCounter.main(args);
		assertTrue(true);
	}

}
