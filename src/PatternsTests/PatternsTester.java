package PatternsTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
//if this gets any bigger, might want to use Reflections to get and run all test classes
@RunWith(Suite.class)
@Suite.SuiteClasses(
		{TestCountingDictionary.class, 
			TestTextFileReader.class, 
			TestPatternMatcher.class, 
			TestPatternCounter.class})
public final class PatternsTester {

}

