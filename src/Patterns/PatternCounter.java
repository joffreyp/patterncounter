package Patterns;

import java.io.File;
import java.io.IOException;
/**Main class for pattern counting package.
 * Includes path to pattern file, and takes user input to select a
 * data source, and pattern number before returning matches with
 * their respective counts to the console.
 * 
 * @author joffrey
 *
 */
public class PatternCounter {
	//path to the pattern file
	private static String patternFilePath = "src\\patterns.txt";

	/**Main method - takes two arguments from user 
	 * to provide data to be matched, and pattern choice.
	 * Prints matches and their respective counts to the console.
	 * 
	 * @param args first argument is a path to a text file containing the text to be matched
	 * second argument is an integer, denoting the pattern choice:
	 * 1 - matches words composed of letters
	 * 2 - matches numbers without letters
	 * 3 - matches unique 3-word combinations, where each word can be composed of letters and/or numbers
	 */
	public static void main(String[] args) {
		try{
			//check that inputs are reasonable
			if(args != null && args.length == 2 && Integer.parseInt(args[1]) > 0 && Integer.parseInt(args[1]) < 4){
				File testFile  = new File(args[0]);
		
				if(!testFile.exists()){
					//bad user input!
					throw new IOException("Input must specify a valid file path.");
				}
			}
				
			//create new PatternMatcher with input data file in args[0], and the pattern in args[1]				
			PatternMatcher pm = new PatternMatcher(args[0], patternFilePath, Integer.parseInt(args[1]));
			//count pattern matches
			pm.countPattern();
			//get the data
			CountingDictionary dict = pm.getDictionary();
			
			//printing to console, as per my interpretation of the instructions
			System.out.println(dict.toString());

			return;
		}
		catch (Exception e){
			//something has gone horribly wrong! print error, stacktrace & exit.
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(-1);
		}
	}

}
