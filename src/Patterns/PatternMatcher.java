package Patterns;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/** Houses pattern matching machinery. Give input data, a file with RegEx patterns,
 * which line from that file contains the desired pattern followed by a space, then
 * the RegEx group from which to count, and countPattern() will count
 * matches to the input pattern.
 * Format example:
 * \b[\D_+]\b 0
 * 
 * @author joffrey
 *
 */
public class PatternMatcher{

	private String inputDataPath;
	private String inputPatternsPath;
	
	private TextFileReader inputData;
	private TextFileReader inputPatterns;
	
	private Pattern pattern;
	private Matcher match;
	
	private int patternNum;
	private int patternGroup;
	
	private CountingDictionary dict;

	//Set up PatternMatcher, retrieve data and pattern
	/**
	 * 
	 * @param inputDataPath String encoding path to input text to be pattern-matched.
	 * @param inputPatternsPath String encoding path to pattern file, each line should
	 * 			contain a RegEx expression, then a space, followed by
	 * 			an integer indicating which RegEx grouping to count
	 * @param patternNumber indicates the line of the pattern file indicated by 
	 * 			inputPatternsPath to be used.
	 * @throws IOException
	 */
	public PatternMatcher(String inputDataPath, String inputPatternsPath, int patternNumber) throws IOException{
		this.inputDataPath = inputDataPath;
		this.inputPatternsPath = inputPatternsPath;
		this.dict = new CountingDictionary();
		this.patternNum = patternNumber - 1;
		getPattern();
		getInputData();
	}
	
	//using the inputPatternsPath and patternNumber from the constructor, create a Pattern,
	//and get the pattern group to count
	private void getPattern() throws IOException{
		try{
			inputPatterns = new TextFileReader(inputPatternsPath);
			
			//need to get a specific subgroup of the RegEx, so we store it in the pattern file on the same
			//line as the pattern, separated by whitespace.
			String[] patterns = inputPatterns.getLine(patternNum).split("\\s+");
			//first element is the RegEx pattern
			pattern = Pattern.compile(patterns[0]);
			//second element is which group to select
			patternGroup = Integer.parseInt(patterns[1]);
						
		}
		catch (IOException ioe){
			System.out.println(ioe.getMessage());
			throw ioe;
		}
	}
	//reads in the text file to be matched against
	private void getInputData() throws IOException{
		try{
			inputData = new TextFileReader(inputDataPath);
			}
		catch (IOException ioe){
			System.out.println(ioe.getMessage());
			throw ioe;
		}
	}
	
	/**Run this to count the pattern matches in the input file using 
	 * the pattern specified in the constructor.
	 * 
	 * @throws IOException
	 */
	public void countPattern() throws IOException{
		if(pattern == null){
			getPattern();
		}
		if(inputData == null){
			getInputData();
		}
		
		match = pattern.matcher(inputData.getFileText());
		
		while (match.find()) {
			String matches = match.group(patternGroup);
			dict.put(matches.replaceAll("[\\n|\\r+]", " ").replaceAll("\\s+", " "));
	     }
	}
	
	/** Returns a CountingDictionary which contains the counted pattern matches
	 * 
	 * @return
	 */
	public CountingDictionary getDictionary(){
		return dict;
	}
	
	

}
