package Patterns;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.lang.Integer;

/** Dictionary which keeps only a single entry per dictionary key,
 * and which increments the value for each time the key is put in,
 * keeping a running total of  the number of times a given key has been
 * put in the dictionary.
 * 
 * @author joffrey
 *
 */
public class CountingDictionary{


	private LinkedHashMap<String, Integer> data;
	
	/**Default constructor.
	 * 
	 */
	public CountingDictionary(){
		data = new LinkedHashMap<String, Integer>();
	}
	
	/** Tries to insert key, value mapping
	 * 
	 * @param key String dictionary word
	 * @param value Number of instances
	 * @return previous value in dictionary for key, or null if not present
	 */
	public Integer put(String key){
		Integer oldValue = data.get(key);
		
		if(data.containsKey(key)){
			//add one to the value, if the key exists
			data.replace(key, new Integer(data.get(key).intValue() + 1));
		}
		else{
			data.put(key, 1);
		}
	
		return oldValue;
	}
	
	/**Get the value for a particular key String.
	 * 
	 * @param key dictionary key to look up.
	 * @return Integer corresponding to key in dictionary.
	 */
	public Integer get(String key){
		return data.get(key);
	}
	
	/**Returns true if the dictionary contains the String key.
	 * 
	 * @param key dictionary key to look up.
	 * @return true if key is stored in dictionary, false otherwise.
	 */
	public boolean containsKey(String key){
		return data.containsKey(key);
	}
	
	/**Checks if the dictionary is empty.
	 * 
	 * @return true if dictionary is empty; false otherwise.
	 */
	public boolean isEmpty(){
		return data.isEmpty();
	}
	
	/**
	 * Returns String with entire dictionary formatted as CSV.
	 */
	public String toString(){
		String output = "";
		Set<String> keys = data.keySet();
		
		Iterator<String> it = keys.iterator();
		while(it.hasNext()){
			String n = it.next();
			output = output + n + ", " + data.get(n) + System.lineSeparator();
		}

		return output.trim();
	}
}
