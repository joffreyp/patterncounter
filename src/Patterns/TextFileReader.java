package Patterns;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/** Wraps text file reading for the pattern matching package
 * 
 * @author joffrey
 *
 */
public class TextFileReader {

	private List<String> fileLines;

	/**Sets up a TextFileReader object with a path to a file, and
	 * reads in the file.
	 * 
	 * @param filePath path to text file to be read.
	 * @throws IOException for bad file path, or file access issues
	 */
	public TextFileReader(String filePath) throws IOException{
		readFile(filePath);
	}
	
	
	//simple reader implemented; works well for small files
	private void readFile(String path) throws IOException{ 
		try{
			//read all lines in to a List of Strings; readAllLines closes closes the file.
			fileLines = Files.readAllLines(Paths.get(path));
		}
		catch (IOException ioe){
			ioe.printStackTrace();
			throw ioe;
		}
	}
	
	
	
	//Getters
	/**Returns the entire text file text as a String.
	 * 
	 * @return
	 */
	public String getFileText(){
		//return file as single String, reinserting platform-specific line separator.
		return fileLines.stream().collect(Collectors.joining(System.lineSeparator()));
	}
	/**Returns a given line from the input text file as a String.
	 * 
	 * @param line line of input file to be read.
	 * @return line of text at line position.
	 */
	public String getLine(int line){
		//return a specific line from file
		return fileLines.get(line);
	}

}
