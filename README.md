# README #

This project was created with Eclipse. Import to Eclipse for full functionality.

This project is my solution to an exercise for a job application. The full description is below.

### Running the .jar ###

*Execute: 
java -cp PatternCounter.jar Patterns.PatternCounter src\PatternsTests\testText1.txt 1
or exchange the file for one of your own choosing, and replace the 1 with any of {1,2,3} 
to test against the three patterns described below.


### Exercise Description ###

Please complete this programming exercise in Java and send me the source (java) and test (junit) code. If you have questions about the details of specifications/requirements, I would prefer that you make an assumption, and just state all assumptions you made in the email response. But if you are really stuck with something, feel free to email me questions.
 
The submission is evaluated based on following:
1.	Proper use of object oriented design to ensure extendibility. As an example, how much would the design have to change if 10 new patterns were to be added?
2.	Loose coupling between input source, core logic, and output source.
3.	Unit tests that test behavior instead of just methods.
4.	Program should be packaged such that no setup is required to make it run.
 
Write a program (source and unit tests) "PatternCounter" that reads an input document from a file (path provided as first command line argument) and calculates the number of occurrences of one of the 3 patterns based on the second command line argument:
1: Counts occurrences of each unique word in the document
2: Counts occurrences of each unique number in the document
3: Counts occurrences of each unique phrase of three consecutive words in the document
 
It then prints each word, number or phrase and its count on standard output separated by a comma in a new line. For the purpose of this exercise, use space character as the de-limiter for words. String matching should be case sensitive. Here are some examples of output with a sample input file using different arguments:
 
Contents of Input document (Input.txt): "1000 a big surprise 2000 hello is a big surprise 1000"
 
PatternCounter Input.txt 1
a, 2
big, 2
surprise, 2
hello, 1
is, 1
 
PatternCounter Input.txt 2
1000, 2
2000, 1
 
PatternCounter Input.txt 3
1000 a big, 1
a big surprise, 2
big surprise 2000, 1
surprise 2000 hello, 1
2000 hello is, 1
hello is a, 1
is a big, 1
big surprise 1000, 1
 
The output could be empty if there are less than 3 words in the document. Please include all source and test files in the email response.